from diagrams import Cluster, Diagram, Edge
from diagrams.aws.network import ELB
from diagrams.onprem.monitoring import Grafana, Prometheus
from diagrams.onprem.network import Nginx
from diagrams.onprem.client import Client
from diagrams.gcp.analytics import Dataflow
from diagrams.onprem.ci import GitlabCI
from diagrams import Cluster, Diagram
from diagrams.aws.compute import ECS
from diagrams.aws.database import ElastiCache, RDS
from diagrams.aws.network import Route53
from diagrams.elastic.elasticsearch import ElasticSearch, Logstash
from diagrams.programming.language import Go
from diagrams.onprem.logging import Rsyslog, Loki

with Diagram("SkillBox Project Advanced", show=False):
    gitlab = GitlabCI("gitlab.com")
#    runner = GitlabCI("Local Runner")
#    client = Client("Gitlab Runner")
    
    with Cluster("Yandex Cloud"):
        with Cluster("VM Monitoring"):
            with Cluster("Docker"):
                with Cluster("Gitlab Runner"):
                    runner = GitlabCI("Gitlab Runner")


            primary = Prometheus("Prometheus")
            secondary = Grafana("Grafana")
        #    tertiary = Loki("Loki")
            primary - secondary 

        with Cluster("VM Service"):
            with Cluster("Docker"):
                with Cluster("Service"):
                    service = Go("Skillboxapp")
        #    rsyslog = Rsyslog("Logging")

        elb = ELB("Yandex App Load Balancer")
#        tertiary << rsyslog
        primary >> elb >> service
    
    gitlab >> runner
#    monitoring >> service
#    client >> monitoring
