# Финальная работа курса «DevOps-инженер. Advanced»

## Статус

Рекомендован к использованию

## Задача

Развернуть динамическую группу ВМ с сервисом, мониторинг сервиса, кластер Elasticsearch и Sonarqube в облачной среде Yandex Cloud и интегрировать с Gitlab-CI

## Решение 

### Топология

![Topology](png/final.png?raw=true "Skillbox Advanced")

### Запуск и останов проекта

1) Склонируйте проект 

```
git clone https://gitlab.com/sb721407/advanced/sb_a_final.git
```

и перейдите в корневую директорию проекта:

```
cd sb_a_final
```

2) Создайте новый проект в Gitlab

3) Измените переменную reg_token в файле gitlab-runner/tf/vars.yml на соответствующую из раздела Gitlab - Project - Settings - CI/CD - Runners - Specific runners
вашего нового проекта

4) Запустите развертывание и настройку gitlab-runner, ELK-стека и Sonarqube:
```
./rollout.sh start
```

5) В новом проекте Gitlab укажите переменные в разделе Gitlab - Project - Settings - CI/CD Variables: 

- ID_RSA (ключ SSH для подключения к инфраструктуре в облаке) 
- YANDEX_OAUTH (OAuth-токен для управления облаком Yandex)
- DOCKER_CONFIG_JSON (конфигурация подключения к реестру Docker-образов),
например
```
{
        "auths": {
                "https://index.docker.io/v1/": {
                        "auth": "cGV0cmFraM1vdmRsY2tlcbpVbmtub3duKzg5"
                }
        }
}
```
- GL_ACCESS_TOKEN (токен доступа к Gitlab)
- SONAR_HOST_URL (URL Sonarqube)
- SONAR_TOKEN (Токен Sonarqube)

6) В директориях main и develop переименуйте файлы с расширением dist, убрав дополнительное расширение dist и указав нужные параметры

7) Укажите git origin:
```
git remote add origin <ссылка на ваш проект в Gitlab>
```

8) Отправьте проект в Gitlab вручную: 
```
git add .; git commit -m "commit descrption"; git push -uf origin main;
```

или воспользуйтесь готовыми bash-скриптами:

- push.sh
- push-dev.sh
- push-tag.sh

9) Дождитесь окончания работы джоб, запускаемых автоматически. 

10) Важные ссылки для работы с проектом

Для обеих веток:
- http://elk1.akimov.space:5601 (интерфейс Kibana)
- http://sonar.akimov.space:9000 (интерфейс Sonarqube)

Для ветки main:
- http://mon1.akimov.space:3000/login (интерфейс Grafana)
- http://mon1.akimov.space:8500/ui/dc1/nodes (интерфейс Consul)
- http://alb1.akimov.space:8080/ (сервис)
- http://mon1.akimov.space:8428 (интерфейс Victoria Metrics)

Для ветки develop:
- http://mon2.akimov.space:3000/login
- http://mon2.akimov.space:8500/ui/dc1/nodes
- http://alb2.akimov.space:8080/
- http://mon2.akimov.space:8428

11) Для удаления окружения develop/main проекта вручную запустите джобу destroy

12) Для удаления облачных серверов Gitlab-Runner, Sonarqube и кластера Elasticsearch
выполните:

```
./rollout.sh stop
```

### Структура каталогов
```
├── gitlab-runner
│   ... (код для развертывания Gitlab-Runner, ELK-стека и Sonarqube)
├── README.md
├── rollout.sh (скрипт для развертывание и удаления Gitlab-Runner, ELK-стека и Sonarqube)
├── push.sh, push-dev.sh, push-tag.sh (три сценария запуска пайплайна: ветка main без тегов, ветка develop, ветка main с тегами)
├── sonar-project.properties (вспомогательный файл для проверки Sonarqube - измените в соответствии с п. 13)
├── png
│   ... (скриншоты, топологии, рисунки)
├── src
│   ... (конфиги для сервиса, мониторинга, системы сбора логов)
├── develop
│   ... (IaC для развертывания проекта в ветке develop)
└── main
    ... (IaC для развертывания проекта в ветке main)
```

13) Инструкция по настройке Sonarqube

!!! Выполните данную инструкцию, чтобы корректно отработала джоба check !!!
!!! пайплайна. Пайплайн не будет успешно завершен, если джоба check  не !!!
!!! отработает                                                          !!! 

- Авторизуйтесь в Web-интерфейсе Sonarqube http://sonar.akimov.space:9000 с атрибутами доступа по умолчанию (login/passwd: admin/admin) и установите свой пароль для пользователя admin
- Вы будете автоматически переадресованы на страничку создания нового проекта
- Выберите From GitLab
- В поле Configuration name укажите имя конфигурации. В поле GitLab API URL укажите https://gitlab.com/api/v4 . В поле Personal Access Token укажите значение переменной GL_ACCESS_TOKEN
- Нажмите Save configuration. 
- На странице Gitlab project onboarding система предложит ввести personal access token повторно. Введите и нажмите Save
- В списке проектов выберите ваш новый проект и нажмите Set up
- Выберите With GitLab CI
- Нажмите Generate Token и полученный токен занесите в переменную SONAR_TOKEN
- Нажмите Continue
- На вопрос системы о типе сборки проекта выберите Other
- Содержимое пункта 2 скопируйте в буфер обмена и укажите в новом файле sonar-project.properties в корневой директории проекта
- Нажмите Finish

## Результаты

При развертывании проекта сервис, мониторинг, система логирования, вся необходимая инфраструктура и её настройка запускаются автоматически в параллельном режиме.
