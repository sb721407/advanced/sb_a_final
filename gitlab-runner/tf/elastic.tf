##### ELK Cluster #####

resource "yandex_compute_instance" "elk1" {
  name        = "elk1"
  platform_id = "standard-v2"
  resources {
    core_fraction = 100
    cores         = "4"
    memory        = "8"
  }
  boot_disk {
    initialize_params {
      image_id = "fd8evlqsgg4e81rbdkn7" # ubuntu-22
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-3.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "elk2" {
  name        = "elk2"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "4"
    memory        = "8"
  }
  boot_disk {
    initialize_params {
      image_id = "fd8evlqsgg4e81rbdkn7" # ubuntu-22
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-3.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "elk3" {
  name        = "elk3"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "4"
    memory        = "8"
  }
  boot_disk {
    initialize_params {
      image_id = "fd8evlqsgg4e81rbdkn7" # ubuntu-22
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-3.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

##### ELK DNS A-records #####

resource "yandex_dns_recordset" "elk1_dns_name" {
  depends_on = [yandex_compute_instance.elk1]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "elk1"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.elk1.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "elk2_dns_name" {
  depends_on = [yandex_compute_instance.elk2]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "elk2"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.elk2.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "elk3_dns_name" {
  depends_on = [yandex_compute_instance.elk3]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "elk3"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.elk3.network_interface.0.nat_ip_address]
}

##### Create file elk_inventory #####

resource "local_file" "elk_inventory" {
  content  = <<EOF

[elk1]
${yandex_compute_instance.elk1.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

[elk2]
${yandex_compute_instance.elk2.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

[elk3]
${yandex_compute_instance.elk3.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

EOF
  filename = "${path.module}/elk_inventory"
}

##### Create files elasticsearch.yml #####

resource "local_file" "elastic1" {
  content  = <<EOF
network.host: [_eth0_, _local_]
node.name: elasticsearch-1
cluster.name: elasticsearch_cluster
cluster.initial_master_nodes:
        - elasticsearch-1
        - elasticsearch-2
        - elasticsearch-3
discovery.seed_hosts:
        - ${yandex_compute_instance.elk1.network_interface.0.ip_address}
        - ${yandex_compute_instance.elk2.network_interface.0.ip_address}
        - ${yandex_compute_instance.elk3.network_interface.0.ip_address}
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
EOF
  filename = "${path.module}/elastic-elk1.yml"
}

resource "local_file" "elastic2" {
  content  = <<EOF
network.host: [_eth0_, _local_]
node.name: elasticsearch-2
cluster.name: elasticsearch_cluster
cluster.initial_master_nodes:
        - elasticsearch-1
        - elasticsearch-2
        - elasticsearch-3
discovery.seed_hosts:
        - ${yandex_compute_instance.elk1.network_interface.0.ip_address}
        - ${yandex_compute_instance.elk2.network_interface.0.ip_address}
        - ${yandex_compute_instance.elk3.network_interface.0.ip_address}
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
EOF
  filename = "${path.module}/elastic-elk2.yml"
}

resource "local_file" "elastic3" {
  content  = <<EOF
network.host: [_eth0_, _local_]
node.name: elasticsearch-3
cluster.name: elasticsearch_cluster
cluster.initial_master_nodes:
        - elasticsearch-1
        - elasticsearch-2
        - elasticsearch-3
discovery.seed_hosts:
        - ${yandex_compute_instance.elk1.network_interface.0.ip_address}
        - ${yandex_compute_instance.elk2.network_interface.0.ip_address}
        - ${yandex_compute_instance.elk3.network_interface.0.ip_address}
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
EOF
  filename = "${path.module}/elastic-elk3.yml"
}

##### Provisioning with Ansible VM elk1 #####

resource "null_resource" "elk1" {
  depends_on = [yandex_compute_instance.elk1, local_file.elk_inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.elk1.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i elk_inventory --key-file id_rsa ../ansible/elk1.yml"
  }
}

resource "null_resource" "elk2" {
  depends_on = [yandex_compute_instance.elk2, local_file.elk_inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.elk2.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i elk_inventory --key-file id_rsa ../ansible/elk2.yml"
  }
}

resource "null_resource" "elk3" {
  depends_on = [yandex_compute_instance.elk3, local_file.elk_inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.elk3.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i elk_inventory --key-file id_rsa ../ansible/elk3.yml"
  }
}