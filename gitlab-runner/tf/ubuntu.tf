##### VMs Part #####

resource "yandex_compute_instance" "sonar" {

  name        = "sonar"
  platform_id = "standard-v2"
  #  zone        = "ru-central1-a"

  resources {
    core_fraction = 100
    cores         = "4"
    memory        = "4"
  }

  boot_disk {
    initialize_params {
      #      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      #      image_id = "fd8j9i69vt27ujq6rqug" # centos-8
      image_id = "fd8evlqsgg4e81rbdkn7" # ubuntu-2204
      size     = 20
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-3.id
    nat       = true
  }

  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }

  scheduling_policy {
    preemptible = true
  }
}

##### Create file inventory #####

resource "local_file" "inventory2" {
  content  = <<EOF

[all]
${yandex_compute_instance.sonar.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

EOF
  filename = "${path.module}/inventory2"
}

##### Provisioning #####

resource "null_resource" "sonar" {
  depends_on = [yandex_compute_instance.sonar, local_file.inventory2]

  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.sonar.network_interface.0.nat_ip_address
  }

  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory2 --key-file id_rsa ../ansible/sonar.yml"
  }
}

##### Add A-records to DNS zone #####

resource "yandex_dns_recordset" "sonar_dns_name" {
  depends_on = [yandex_compute_instance.sonar]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "sonar"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.sonar.network_interface.0.nat_ip_address]
}
